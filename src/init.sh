#!/bin/bash

# Check the package manager and install required packages
if command -v apt-get &> /dev/null; then
    # Debian/Ubuntu-based systems
    sudo apt-get update
    sudo apt-get install -y curl wget vim
elif command -v yum &> /dev/null; then
    # Red Hat/Fedora-based systems
    sudo yum install -y curl wget vim
else
    echo "Unsupported package manager. Please install the required packages manually."
    exit 1
fi

# Sleep for 30 seconds
echo "Sleeping for 30 seconds..."
sleep 30


echo "Done"
